package com.hendisantika.cloud.transactionservice.feign

import com.hendisantika.cloud.transactionservice.mapping.Barang
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * Created by IntelliJ IDEA.
 * Project : transaction-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-13
 * Time: 08:11
 */
@FeignClient(value = "catalog", fallback = BarangServiceClientFallback::class)
interface BarangServiceClient {
    @RequestMapping(value = ["/api/barangs"], method = [RequestMethod.GET])
    fun getBarangs(pageable: Pageable): Page<Barang>

    @RequestMapping(value = ["/api/barangs/{id}"], method = [RequestMethod.GET])
    fun getBarang(@PathVariable("id") id: String?): Barang
}