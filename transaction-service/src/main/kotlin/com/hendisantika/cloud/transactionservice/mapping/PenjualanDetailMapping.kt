package com.hendisantika.cloud.transactionservice.mapping

import java.math.BigDecimal

/**
 * Created by IntelliJ IDEA.
 * Project : transaction-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-13
 * Time: 08:10
 */
data class PenjualanDetailMapping(
        val idPenjualanDetail: String? = null,
        val jumlahBarang: Int? = null,
        val totalHargaPerBarang: BigDecimal? = null,
        val idPenjualan: String? = null,
        val barang: Barang? = null
)