package com.hendisantika.cloud.transactionservice.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.math.BigDecimal

/**
 * Created by IntelliJ IDEA.
 * Project : transaction-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-13
 * Time: 08:07
 */
@Document(collection = "tb_penjualan_detail")
data class PenjualanDetail(
        @Id
        @Field
        val idPenjualanDetail: String? = null,
        @Field
        val jumlahBarang: Int? = null,
        @Field
        val totalHargaPerBarang: BigDecimal? = null,
        @Field
        val idPenjualan: String? = null,
        @Field
        val idBarang: String? = null
)