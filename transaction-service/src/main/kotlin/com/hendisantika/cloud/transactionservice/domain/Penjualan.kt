package com.hendisantika.cloud.transactionservice.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.math.BigDecimal
import java.time.LocalDate

/**
 * Created by IntelliJ IDEA.
 * Project : transaction-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-13
 * Time: 07:46
 */
@Document(collection = "tb_penjualan")
data class Penjualan(
        @Id
        @Field
        val idPenjualan: String? = null,
        @Field
        val tanggalTransaksi: LocalDate? = null,
        @Field
        val namaPembeli: String? = null,
        @Field
        val totalHarga: BigDecimal? = null
)