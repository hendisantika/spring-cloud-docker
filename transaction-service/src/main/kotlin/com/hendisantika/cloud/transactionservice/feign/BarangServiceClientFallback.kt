package com.hendisantika.cloud.transactionservice.feign

import com.hendisantika.cloud.transactionservice.mapping.Barang
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component

/**
 * Created by IntelliJ IDEA.
 * Project : transaction-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-13
 * Time: 08:12
 */
@Component
class BarangServiceClientFallback : BarangServiceClient {
    override fun getBarangs(pageable: Pageable): Page<Barang> {
        return PageImpl(ArrayList())
    }

    override fun getBarang(id: String?): Barang {
        return Barang()
    }
}