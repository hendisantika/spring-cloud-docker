package com.hendisantika.cloud.transactionservice.repository

import com.hendisantika.cloud.transactionservice.domain.PenjualanDetail
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : transaction-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-13
 * Time: 08:09
 */
interface PenjualanDetailRepository : MongoRepository<PenjualanDetail, String> {
    fun findByIdPenjualanDetail(idPenjualanDetail: String): Optional<PenjualanDetail>
    fun findByIdPenjualan(idPenjualan: String): List<PenjualanDetail>
}