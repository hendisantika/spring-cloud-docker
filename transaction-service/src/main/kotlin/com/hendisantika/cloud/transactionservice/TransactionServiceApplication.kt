package com.hendisantika.cloud.transactionservice

import com.hendisantika.cloud.transactionservice.domain.Penjualan
import com.hendisantika.cloud.transactionservice.domain.PenjualanDetail
import com.hendisantika.cloud.transactionservice.repository.PenjualanDetailRepository
import com.hendisantika.cloud.transactionservice.repository.PenjualanRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.cloud.openfeign.EnableFeignClients
import java.math.BigDecimal
import java.time.LocalDate

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@RefreshScope
class TransactionServiceApplication : CommandLineRunner {
    @Autowired
    private lateinit var penjualanDetailRepository: PenjualanDetailRepository

    @Autowired
    private lateinit var penjualanRepository: PenjualanRepository

    override fun run(vararg args: String?) {
        for (i in 1..10) {
            penjualanRepository.save(
                    Penjualan(
                            idPenjualan = i.toString(),
                            namaPembeli = "nama ke $i",
                            tanggalTransaksi = LocalDate.now(),
                            totalHarga = BigDecimal(i * 1000)
                    )
            )
        }
        for (i in 1..10) {
            penjualanDetailRepository.save(
                    PenjualanDetail(
                            idPenjualanDetail = "$i.detail",
                            idPenjualan = i.toString(),
                            idBarang = i.toString(),
                            totalHargaPerBarang = BigDecimal(i * 1000),
                            jumlahBarang = i + 5
                    )
            )
        }
    }
}

fun main(args: Array<String>) {
    runApplication<TransactionServiceApplication>(*args)
}

