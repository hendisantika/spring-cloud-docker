package com.hendisantika.cloud.transactionservice.repository

import com.hendisantika.cloud.transactionservice.domain.Penjualan
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : transaction-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-13
 * Time: 08:08
 */
interface PenjualanRepository : MongoRepository<Penjualan, String> {
    fun findByIdPenjualan(idPenjualan: String): Optional<Penjualan>
}