package com.hendisantika.cloud.transactionservice.mapping

/**
 * Created by IntelliJ IDEA.
 * Project : transaction-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-13
 * Time: 08:10
 */

data class Barang(
        val idBarang: Int? = null,
        val namaBarang: String? = null,
        val jenisBarang: String? = null,
        val tanggalKadaluarsa: TanggalKadaluarsa? = null,
        val hargaSatuanBarang: String? = null,
        val jumlahBarangTersedia: Int? = null
)

data class TanggalKadaluarsa(
        val year: Int? = null,
        val month: String? = null,
        val leapYear: Boolean? = null,
        val dayOfMonth: Int? = null,
        val dayOfWeek: String? = null,
        val dayOfYear: Int? = null,
        val era: String? = null,
        val monthValue: Int? = null,
        val chronology: Chronology? = null
)

data class Chronology(
        val calendarType: String? = null,
        val id: String? = null
)