package com.hendisantika.cloud.admindashboard.config

import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.core.env.Environment
import java.nio.charset.Charset
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : admin-dashboard
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-10
 * Time: 09:06
 */
class ZuulFilterConfiguration constructor(private val environment: Environment) : ZuulFilter() {

    override fun run(): Any? {
        val requestContext = RequestContext.getCurrentContext()
        val usernamePassword = "${environment.getRequiredProperty("spring.boot.admin.client.metadata.user.name")}:${environment.getRequiredProperty("spring.boot.admin.client.metadata.user.password")}"
        val usernamePasswordBase64 = Base64.getEncoder().encodeToString(usernamePassword.toByteArray(Charset.defaultCharset()))
        requestContext.addZuulRequestHeader("Authorization", "Basic $usernamePasswordBase64")
        return null
    }

    override fun shouldFilter(): Boolean {
        return true
    }

    override fun filterType(): String {
        return "pre"
    }

    override fun filterOrder(): Int {
        return 1
    }
}
