package com.hendisantika.cloud.admindashboard.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment

/**
 * Created by IntelliJ IDEA.
 * Project : admin-dashboard
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-10
 * Time: 09:13
 */
@Configuration
class ZuulConfiguration @Autowired constructor(val environment: Environment) {

    @Bean
    fun zuulFilterConfiguration(): ZuulFilterConfiguration {
        return ZuulFilterConfiguration(environment)
    }

}