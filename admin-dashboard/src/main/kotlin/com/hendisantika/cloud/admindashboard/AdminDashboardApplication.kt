package com.hendisantika.cloud.admindashboard

import de.codecentric.boot.admin.server.config.EnableAdminServer
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.context.config.annotation.RefreshScope

@SpringBootApplication
@EnableDiscoveryClient
@EnableAdminServer
@RefreshScope
class AdminDashboardApplication

fun main(args: Array<String>) {
    runApplication<AdminDashboardApplication>(*args)
}

