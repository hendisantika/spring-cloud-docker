package com.hendisantika.cloud.oauth2service.repository

import com.hendisantika.cloud.oauth2service.domain.OAuth2ClientDetail
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : oauth2-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-12
 * Time: 07:24
 */
interface OAuth2ClientDetailRepository : PagingAndSortingRepository<OAuth2ClientDetail, String> {
    fun findByClientId(clientId: String): Optional<OAuth2ClientDetail>
}