package com.hendisantika.cloud.oauth2service.domain

import javax.persistence.*

/**
 * Created by IntelliJ IDEA.
 * Project : oauth2-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-12
 * Time: 07:21
 */
@Entity
@Table(name = "tb_user_oauth")
data class User(
        @Id
        @Column
        val username: String? = null,
        @Column
        val password: String? = null,
        @Column(name = "is_active")
        val isActive: Boolean? = null,
        @ElementCollection(fetch = FetchType.EAGER)
        @CollectionTable(name = "roles", joinColumns = [(JoinColumn(name = "username"))])
        @Column(name = "roles")
        val roles: Set<String>? = null
)