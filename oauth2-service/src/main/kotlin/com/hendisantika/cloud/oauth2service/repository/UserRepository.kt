package com.hendisantika.cloud.oauth2service.repository

import com.hendisantika.cloud.oauth2service.domain.User
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : oauth2-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-12
 * Time: 07:23
 */
interface UserRepository : PagingAndSortingRepository<User, String> {
    fun findByUsername(username: String): Optional<User>
}