package com.hendisantika.cloud.oauth2service

import com.hendisantika.cloud.oauth2service.domain.OAuth2ClientDetail
import com.hendisantika.cloud.oauth2service.domain.User
import com.hendisantika.cloud.oauth2service.repository.OAuth2ClientDetailRepository
import com.hendisantika.cloud.oauth2service.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@SpringBootApplication
@EnableDiscoveryClient
@RefreshScope
class Oauth2ServiceApplication : CommandLineRunner {
    @Autowired
    private lateinit var userRepositoty: UserRepository

    @Autowired
    private lateinit var oAuth2ClientDetailRepository: OAuth2ClientDetailRepository

    override fun run(vararg args: String?) {
        if (!this.userRepositoty.findByUsername("rizki").isPresent) {
            this.userRepositoty.save(User(
                    username = "naruto",
                    password = BCryptPasswordEncoder().encode("naruto"),
                    isActive = true,
                    roles = setOf("ROLE_ADMIN", "ROLE_USER")
            ))

            this.userRepositoty.save(User(
                    username = "actuator",
                    password = BCryptPasswordEncoder().encode("actuator"),
                    isActive = true,
                    roles = setOf("ACTUATOR", "ROLE_MANAGE")
            ))
        }

        if (!this.oAuth2ClientDetailRepository.findByClientId("clientid").isPresent) {
            this.oAuth2ClientDetailRepository.save(OAuth2ClientDetail(
                    clientId = "clientid",
                    resourceIds = "RESOURCE_ID_API_GATEWAY",
                    clientSecret = "secret",
                    scope = "read,write",
                    authorizedGrantTypes = "client_credentials,password,refresh_token",
                    webServerRedirectUri = " ",
                    authorities = "ADMIN,CLIENT,ADMINISTRATOR",
                    accessTokenValidity = 3600,
                    refreshTokenValidity = 3600,
                    additionalInformation = "{\"additional_param\":\"hello OAuth2\"}",
                    autoApprove = true
            ))
        }

    }
}

fun main(args: Array<String>) {
    runApplication<Oauth2ServiceApplication>(*args)
}

