package com.hendisantika.cloud.catalogservice.domain

/**
 * Created by IntelliJ IDEA.
 * Project : catalog-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-10
 * Time: 09:29
 */
enum class JenisBarang {
    gas, cair, padat
}