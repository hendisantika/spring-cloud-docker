package com.hendisantika.cloud.catalogservice.controller

import com.hendisantika.cloud.catalogservice.domain.Barang
import com.hendisantika.cloud.catalogservice.repository.BarangRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * Created by IntelliJ IDEA.
 * Project : catalog-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-10
 * Time: 09:37
 */
@RestController
@RequestMapping(value = ["/api"])
class BarangController @Autowired constructor(val barangRepository: BarangRepository) {

    @GetMapping(value = ["/barangs"])
    fun getBarangs(pageable: Pageable): ResponseEntity<*> {
        return ResponseEntity(barangRepository.findAll(pageable), HttpStatus.OK)
    }

    @GetMapping(value = ["/barangs/{id}"])
    fun getBarang(@PathVariable("id") id: Int): ResponseEntity<*> {
        return ResponseEntity(barangRepository.findByIdBarang(id).orElse(Barang()), HttpStatus.OK)
    }

    @PostMapping(value = ["/barangs"])
    fun saveBarangs(@RequestBody barang: Barang): ResponseEntity<*> {
        return ResponseEntity(barangRepository.save(barang), HttpStatus.OK)
    }

    @PutMapping(value = ["/barangs/{id}"])
    fun updateBarangs(@PathVariable("id") id: Int, @RequestBody barang: Barang): ResponseEntity<*> {
        barang.copy(idBarang = id)
        return ResponseEntity(barangRepository.save(barang), HttpStatus.OK)
    }

    @DeleteMapping(value = ["/barangs/{id}"])
    fun deleteBarangs(@PathVariable("id") id: Int): ResponseEntity<*> {
        return ResponseEntity(barangRepository.deleteById(id), HttpStatus.OK)
    }

}