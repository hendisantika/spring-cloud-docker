package com.hendisantika.cloud.catalogservice.helper

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.io.IOException
import java.time.LocalDate

/**
 * Created by IntelliJ IDEA.
 * Project : catalog-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-10
 * Time: 09:32
 */
class LocalDateDeserializer : JsonDeserializer<LocalDate>() {
    @Throws(IOException::class)
    override fun deserialize(jsonParser: JsonParser?, deserializationContext: DeserializationContext?): LocalDate {
        return LocalDate.parse(jsonParser?.text)
    }
}