package com.hendisantika.cloud.catalogservice.repository

import com.hendisantika.cloud.catalogservice.domain.Barang
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

/**
 * Created by IntelliJ IDEA.
 * Project : catalog-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-10
 * Time: 09:32
 */
interface BarangRepository : PagingAndSortingRepository<Barang, Int> {
    fun findByIdBarang(idBarang: Int): Optional<Barang>
}