package com.hendisantika.cloud.catalogservice.domain

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

/**
 * Created by IntelliJ IDEA.
 * Project : catalog-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-10
 * Time: 09:28
 */
@Entity
@Table(name = "tb_barang")
data class Barang(
        @Id
        @Column(name = "id_barang", length = 36)
        val idBarang: Int? = null,

        @Column(name = "nama_barang", length = 50)
        val namaBarang: String? = null,

        @Enumerated(EnumType.STRING)
        @Column(name = "jenis_barang", length = 5)
        val jenisBarang: JenisBarang? = null,

        @JsonSerialize(using = LocalDateSerializer::class)
        @JsonDeserialize(using = LocalDateDeserializer::class)
        @Column(name = "tanggal_kadaluarsa")
        val tanggalKadaluarsa: LocalDate? = null,

        @Column(name = "harga_satuan_barang")
        val hargaSatuanBarang: BigDecimal? = null,

        @Column(name = "jumlah_barang_tersedia")
        val jumlahBarangTersedia: Int? = null
)